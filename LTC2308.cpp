#import "LTC2308.h"
#include <SPI.h>


#pragma mark -
#pragma mark Public functions

LTC2308::LTC2308(){}

void LTC2308::setup()
{
    // Start the SPI, deselect the chip
   SPI.begin();

   DDRC  |= 1<<3;              // Set pin as an output
   PORTC &= ~(1<<3);           // Set pin 10 LOW
}

measurements LTC2308::read(uint8_t channels)
{
   // Check what channels to measure, add them to the queue
   uint8_t count;
   if(channels & 1<<0){ queue[count] = 0; count++; }
   if(channels & 1<<1){ queue[count] = 1; count++; }
   if(channels & 1<<2){ queue[count] = 2; count++; }
   if(channels & 1<<3){ queue[count] = 3; count++; }
   if(channels & 1<<4){ queue[count] = 4; count++; }
   if(channels & 1<<5){ queue[count] = 5; count++; }
   if(channels & 1<<6){ queue[count] = 6; count++; }
   if(channels & 1<<7){ queue[count] = 7; count++; }

   // Enable communications
   SPI.beginTransaction(SPISettings(1600000, MSBFIRST, SPI_MODE2));
   
   // Start a new measurement struct
   measurements mes;
   static uint8_t firstChannel = queue[0];
 
   // Check if the first channel equals the last channel we read
   if(_lastReadChannel != firstChannel)
      // Send the first request, this will only be written since this will only return data from the last sent request
      this->sendRequest(firstChannel);

   // Check the size of the array, only loop if we have more than one sensor / the last 
   // channel will be read out a bit later
   for(uint8_t i = 0; i < (count-1); i++)
   {
      // We are loading the previous requests result and requesting the new sensor
      mes.write(queue[i], this->sendRequest(queue[i+1]));
   }

   // Request the first sensor again, as there might be a chance we will want it next time!
   _lastReadChannel = queue[0];
   mes.write(queue[count-1], this->sendRequest(queue[0]));

   // Stop the communication
   SPI.endTransaction();

   // Return the number
   return mes;
}




#pragma mark -
#pragma mark Private functions

uint16_t LTC2308::sendRequest(uint8_t channelID)
{
   // Begin the last requested conversion
   PORTC |= 1<<3;              // Set pin HIGH
   PORTC &= ~(1<<3);           // Set pin LOW

   // Prepare
   byte request;

   // Yeah, it can happen, that we are too fast, so let's add a micro delay
   delayMicroseconds(1);

   // Calculate the new request input while the LTC2308 calculates the input
   switch(channelID)
   {
       case 0: request = 0b100010<<2; break;
       case 1: request = 0b110010<<2; break;
       case 2: request = 0b100110<<2; break;
       case 3: request = 0b110110<<2; break;
       case 4: request = 0b101010<<2; break;
       case 5: request = 0b111010<<2; break;
       case 6: request = 0b101110<<2; break;
       case 7: request = 0b111110<<2; break;
       default:
         // Let the user know he has an invalid channel
         #ifdef _SERIALCOM
            Serial.print("Selection of ADC channel is invalid: ");
            Serial.println(channelID);
         #endif
         break;
   }
   
   // Let's talk
   uint8_t one = SPI.transfer(request);
   uint8_t two = SPI.transfer(0b0);

   // Calculate the return value
   return (one << 7 | two) >> 2;    // Shift the first set of data over to the left 
                                    // so we can fit in the last 2 bytes, the get rid
                                    // of the last 2 bytes by shifting the combined number back
} 