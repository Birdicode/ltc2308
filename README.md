# LTC2308
The LTC2308 IC is an 8 channel ADC. It communicates via SPI.
The datasheet can be found here: [Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/2308fc.pdf)

This class is written in C++ for the Arduino.


## Implemented
- Can measure all 8 channels in Single-Ended mode as a Unipolar value


## To be done
- Switch between Single-Ended and differential pair
- Switch between unipolar and Bipolar mode
- Enable sleep mode