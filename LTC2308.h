#ifndef LTC2308_H
#define LTC2308_H

/**
 * Include The libraries we need
 */
#include "Arduino.h"
#include "Definitions.h"

/**
 * @brief      A place where we keep our measurements
 */
struct measurements {
   uint16_t ch0;
   uint16_t ch1;
   uint16_t ch2;
   uint16_t ch3;
   uint16_t ch4;
   uint16_t ch5;
   uint16_t ch6;
   uint16_t ch7;

   /**
    * @brief      Simple channel selector
    *
    * @param[in]  ch    The channel ID we would like to get
    *
    * @return     The value read from that channel
    */
   uint16_t get(uint8_t ch)
   {
      switch(ch)
      {
         case 0: return ch0; break;
         case 1: return ch1; break;
         case 2: return ch2; break;
         case 3: return ch3; break;
         case 4: return ch4; break;
         case 5: return ch5; break;
         case 6: return ch6; break;
         case 7: return ch7; break;
      }
   }

   void write(uint8_t ch, uint16_t value)
   {
      switch(ch)
      {
         case 0: ch0 = value; break;
         case 1: ch1 = value; break;
         case 2: ch2 = value; break;
         case 3: ch3 = value; break;
         case 4: ch4 = value; break;
         case 5: ch5 = value; break;
         case 6: ch6 = value; break;
         case 7: ch7 = value; break;
      }
   }
};

/**
 * Main class to speak with the LTC2308 (ADC)
 */
class LTC2308
{
   private:
      /**
       * The ID of the last read channel, we will use this on the next read to check 
       * if we alredy programmed the LTC2308 to use this channel
       */
      uint8_t _lastReadChannel;

      /**
       * A queue of items waiting to be measured
       */
      uint8_t queue[8];

      /**
       * @brief      Sends a request to the LTC2308
       *
       * @param[in]  sensorNum  The channel ID
       *
       * @return     Returns the value from the last request, so to get a single channel, 
       *             send the same request twice, the data will be sent back in the second request
       */
      uint16_t sendRequest(uint8_t channelID);

   public:

      /**
       * @brief      Constructs the object.
       */
      LTC2308();

      /**
       * @brief      Sets up the class to be used
       */
      void setup();

      /**
       * @brief      Will read an array of channels from the LTC2308
       *
       * @param      channels    An integer containing the sensors we want to read out. 
       *                         1st bit = CH0 (LSB) 
       *                         2nd bit = CH1 
       *                         ... 
       *                         8th bit = CH7 (MSB)
       *
       * @return     a struct with an array containing all of the readings, mapped to the channel ID
       */
      measurements read(uint8_t channels);
};

#endif
